import { FormGroup, FormControl } from '@angular/forms';

/**
 * Valida que el un email ingresado sea correcto
 * @param control Email ingresado
 */
export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}

/**
 * Valida que la contraseña tenga al menos entre 6 y 16 caracteres, 
 * al menos un dígito, al menos una minúscula y al menos una mayúscula y un simbolo
 * @param control Contraseña ingresada
 */
export function passwordValidator(control: FormControl): { [key: string]: any } {
    var passwordRegexp = /^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{6,16}$/;
    if (control.value && !passwordRegexp.test(control.value)) {
        return { invalidPassword: true };
    }
}

/**
 * Valida que las contraseñas ingresadas coincidan
 * @param passwordKey Contraseña
 * @param passwordConfirmationKey Confirmación de contraseña
 */
// export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
//     return (group: FormGroup) => {
//         let password = group.controls[passwordKey];
//         let passwordConfirmation = group.controls[passwordConfirmationKey];
//         if (password.value !== passwordConfirmation.value) {
//             return passwordConfirmation.setErrors({ mismatchedPasswords: true })
//         }
//     }
// }