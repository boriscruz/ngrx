import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

// Modules
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppRoutingModule } from '@app/app.routing';

// Spinner
import { AppComponent } from '@app/app.component';

// pwa
import { ServiceWorkerModule } from '@angular/service-worker';

// environment
import { environment } from '@env/environment';

// components
import { AvailableForAllComponent } from '@app/available-for-all.component';

@NgModule({
  declarations: [
    AppComponent,
    AvailableForAllComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
