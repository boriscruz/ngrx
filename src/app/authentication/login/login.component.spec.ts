import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

// utils
import { loginsFails, loginsSuccess } from '@app/resources/authentication-resource/mocks/logins.mocks';

// components
import { LoginComponent } from './login.component';

// models
import { Login } from '@app/resources/authentication-resource/models/login.model';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        ReactiveFormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('Crear formulario de login', () => {
    expect(component.form.contains('email')).toBeTruthy();
    expect(component.form.contains('password')).toBeTruthy();
  });

  it('Campos requeridos para ingresar a la app: username, password', () => {
    const emailControl = component.form.get('email');
    const passwordControl = component.form.get('password');
    emailControl.setValue('');
    passwordControl.setValue('');
    expect(emailControl.valid).toBeFalsy();
    expect(passwordControl.valid).toBeFalsy();
  });

  it('El password debe tener como minimo 6 caracteres y maximo 16, una letra minuscula y una mayuscula, un numero y un simbolo', () => {
    const fails: Login[] = loginsFails;
    const success: Login[] = loginsSuccess;

    const form = component.form;
    const emailControl = component.form.get('email');
    const passwordControl = component.form.get('password');

    fails.forEach(fail => {
      emailControl.setValue(fail.email);
      passwordControl.setValue(fail.password);
      expect(form.valid).toBeFalsy();
    });

    success.forEach(success => {
      emailControl.setValue(success.email);
      passwordControl.setValue(success.password);
      expect(form.valid).toBeTruthy();
    });

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('Habilitar boton de acceso cuando se ingrese email y password', () => {
    expect(component).toBeTruthy();
  });
});
