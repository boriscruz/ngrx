import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { emailValidator, passwordValidator } from '@app/shared/utils/validators.util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.form = this.fb.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email,
        emailValidator
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.min(6),
        Validators.max(16),
        passwordValidator
      ]))
    });
  }

}
