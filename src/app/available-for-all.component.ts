import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-available-for-all',
  template: `
  <router-outlet></router-outlet>
  `
})
export class AvailableForAllComponent { }
