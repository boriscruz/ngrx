export class Login {
    email: string;
    password: string;
    constructor(data: {
        email?: string;
        password?: string;
    }) {
        this.email = data.email || undefined;
        this.password = data.password || undefined;
    }
}