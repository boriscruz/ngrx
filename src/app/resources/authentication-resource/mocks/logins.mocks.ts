import { Login } from "@auth-resource/models/login.model";

export const loginsSuccess: Login[] = [
    {
        email: 'rbrs1234@gmail.com',
        password: 'doRS%TResk123'
    },
    {
        email: 'fjsk1234@gmail.com',
        password: 'GTd#r3Ek123'
    }
];

export const loginsFails: Login[] = [
    {
        email: 'example@hotmail.com',
        password: 'do'
    },
    {
        email: 'example@hotmail.com',
        password: 'd124$'
    },
    {
        email: 'exampl',
        password: '1234TAds'
    },
    {
        email: 'exampl@yahoo.com',
        password: 'jodiksiwe'
    },
    {
        email: 'exampl@yahoo.com',
        password: 'jodiksiwe123231316'
    }
];