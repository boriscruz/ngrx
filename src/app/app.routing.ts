import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { AvailableForAllComponent } from './available-for-all.component';

const routes: Routes = [
  {
    path: 'autenticacion', component: AvailableForAllComponent, children: [
      { path: '', loadChildren: () => import('src/app/authentication/authentication.module').then(m => m.AuthenticationModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
